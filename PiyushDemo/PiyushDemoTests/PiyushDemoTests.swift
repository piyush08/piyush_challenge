//
//  PiyushDemoTests.swift
//  PiyushDemoTests
//
//  Created by Apple on 28/08/21.
//

import XCTest
import CoreLocation

@testable import PiyushDemo

class PiyushDemoTests: XCTestCase {
    
    var coreDataManager: CoreDataManager!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        coreDataManager = CoreDataManager.sharedManager
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // MARK: CoreData test cases
    
    func test_init_coreDataManager(){
        
        let instance = CoreDataManager.sharedManager
        XCTAssertNotNil( instance )
    }
    
    func test_coreDataStackInitialization() {
        let coreDataStack = CoreDataManager.sharedManager.persistentContainer
        XCTAssertNotNil( coreDataStack )
    }
    
    func test_create_Location() {
        
        let name1 = "first"
        let coordinate1 = CLLocationCoordinate2D(latitude: Double(22.4661), longitude: Double(73.0161))
        
        let name2 = "second"
        let coordinate2 = CLLocationCoordinate2D(latitude: Double(51.5074), longitude: Double(-0.1278))
        
        let name3 = "Third"
        let coordinate3 = CLLocationCoordinate2D(latitude: Double(47.3769), longitude: Double(8.5417))
        
        let location1 = coreDataManager.insertLocation(name: name1, coordinate: coordinate1)
        XCTAssertNotNil( location1 )
        
        let location2 = coreDataManager.insertLocation(name: name2, coordinate: coordinate2)
        XCTAssertNotNil( location2 )
        
        let location3 = coreDataManager.insertLocation(name: name3, coordinate: coordinate3)
        XCTAssertNotNil( location3 )
        
    }
    
    func test_fetch_all_person() {
        let results = coreDataManager.fetchAllLocations()
        XCTAssertEqual(results?.count, 3)
    }    
    
    //MARK:- Whether data Test Case
    func testFetchWhether() {
        let exp = expectation(description:"fetching whether data")
        
        let session: URLSession = URLSession(configuration: .default)
        let url = URL(string:"http://api.openweathermap.org/data/2.5/weather?lat=0&lon=0&appid=fae7190d7e6433ec3a45285ffcf55c86")
        session.dataTask(with: url!) { data, response, error in
            XCTAssertNil(error)
            exp.fulfill()
        }.resume()
        waitForExpectations(timeout: 10.0) { (error) in
            print(error?.localizedDescription ?? "error")
        }
    }
    
    func testfetchingWhetherData() {
        let exp = expectation(description:"fetching whether data")
        
        APICall().getWhetherData(lat: 22.1165, long: 73.1245) { result in
            switch result {
            case .success(let whether):
                XCTAssertNotNil(whether, "the whehther data is nil")
                
                exp.fulfill()
            case .failure(let error):
                XCTAssertNotNil(error)
                exp.fulfill()
                
            }
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
}
