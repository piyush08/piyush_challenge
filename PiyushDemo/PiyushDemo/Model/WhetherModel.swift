//
//  WhetherModel.swift
//  PiyushDemo
//
//  Created by Apple on 29/08/21.
//

import Foundation


import Foundation

// MARK: - Welcome
struct WhetherModel: Codable {
  //  let coord: Coord
    let weather: [Weather]
    let main: Main
    //let timezone, id: Int
    let name: String
}

// MARK: - Coord
//struct Coord: Codable {
//    let lon, lat: Double
//}

// MARK: - Main
struct Main: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, humidity: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure, humidity
    }
}


// MARK: - Weather
struct Weather: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

