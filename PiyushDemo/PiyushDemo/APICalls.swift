//
//  APICalls.swift
//  PiyushDemo
//
//  Created by Apple on 28/08/21.
//

import Foundation


enum myError: Error {
    case inValidURLError
    case noDataError
}

class APICall  {
    
     let BASEURL = "http://api.openweathermap.org/data/2.5/weather"
     let apiKey = "fae7190d7e6433ec3a45285ffcf55c86"
    
    let unit = UserDefaults.standard.string(forKey: "Units") ?? "metric"
    
    func getWhetherData(lat:Double,long:Double, completion: @escaping (Result<WhetherModel,Error>)->Void) {
        
        let urlString = BASEURL + "?appid=\(apiKey)&units=\(unit)" + "&lat=\(lat)&lon=\(long)"
        
        guard let url = URL(string: urlString) else {
            completion(.failure(myError.inValidURLError))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let err = error {
                debugPrint("Loading error: \(err.localizedDescription)")
                completion(.failure(err))
            }else{
                guard let data = data else {
                    completion(.failure(myError.noDataError))
                    return
                }
                
                do {
                    let whetherModel =  try JSONDecoder().decode(WhetherModel.self, from: data)
                    completion(.success(whetherModel))
                } catch let err {
                    completion(.failure(err))
                }
            }
        }.resume()
        
    }
}
