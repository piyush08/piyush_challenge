//
//  DetailsVC.swift
//  PiyushDemo
//
//  Created by Apple on 29/08/21.
//

import UIKit

class DetailsVC: UIViewController {
    
    @IBOutlet weak var lbl_whetherType:UILabel!
    @IBOutlet weak var lbl_whetherDesc:UILabel!
    @IBOutlet weak var lbl_humidity:UILabel!
    @IBOutlet weak var lbl_temp:UILabel!
    @IBOutlet weak var lbl_pressure:UILabel!

    var location : Locations!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = location.name
        self.getData()
    }
    
    func getData() {
        
        WhetherViewModel().fetchWhetherData(loc: location) { [unowned self] result in
            switch result {
            case .success(let whether):
                //debugPrint("success")
                DispatchQueue.main.async {
                    setupData(whther: whether)
                }
                
            case .failure(let err):
                debugPrint(err.localizedDescription)
            }
        }
    }
    
    func setupData(whther: WhetherModel) {
        
        if  !whther.weather.isEmpty {
            let type = whther.weather[0].main
            lbl_whetherType.text = type
            
            let desc = whther.weather[0].weatherDescription
            lbl_whetherDesc.text = desc
        }
        
         let mainWhether = whther.main
        lbl_temp.text = "\(mainWhether.temp)"
        lbl_humidity.text = "\(mainWhether.humidity)"
        lbl_pressure.text = "\(mainWhether.pressure)"
 
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
