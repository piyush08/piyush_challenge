//
//  ViewController.swift
//  PiyushDemo
//
//  Created by Apple on 28/08/21.
//

import UIKit
import CoreData

fileprivate typealias LocationsDataSource = UITableViewDiffableDataSource<Int, Locations>
fileprivate typealias LocationsSnapshot = NSDiffableDataSourceSnapshot<Int, Locations>

class HomeVC: UIViewController {
    
    @IBOutlet weak var tblView : UITableView!
    
    private var dataSource: LocationsDataSource!

    let viewModel = ViewModel()
    
    let searchController = UISearchController(searchResultsController: nil)

    var isSearching : Bool  {
        if searchController.isActive, let text = searchController.searchBar.text, !text.isEmpty{
            return true
        }
        return false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configureDataSource()
        self.setupViewModel()
        self.setupSearchController()
        self.tblView.tableFooterView = UIView(frame: .zero)
    }
    
    
    
    func setupViewModel() {
        
        let arr = viewModel.getAllLocations()
        if !arr.isEmpty {
            self.createSnapshot()
        }
        
        viewModel.addLocation = { [unowned self] loc in
            self.createSnapshot()
        }
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Bookmark"
        tblView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
    
    @IBAction func addLocationPress(){
        self.performSegue(withIdentifier: "addLoc", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addLoc" {
            let addLoc = segue.destination as! AddLocationVC
            addLoc.viewModel = self.viewModel
        }else if segue.identifier == "GotoDetails" {
            let detilsVC = segue.destination as! DetailsVC
            detilsVC.location = sender as? Locations
        }
    }


}

//MARK:- Tableview Delegate
extension HomeVC : UITableViewDelegate {
    
    
    func configureDataSource() {
        dataSource = LocationsDataSource(tableView: tblView) { (tableView, indexPath, Location) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = Location.name
            return cell
        }
    }
    
    private func createSnapshot() {
        var snapshot = LocationsSnapshot()
        snapshot.appendSections([0])
        snapshot.appendItems( isSearching ? self.viewModel.arrFilterLocations : self.viewModel.arrLocations)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint(#function)
        guard let loc = dataSource.itemIdentifier(for: indexPath) else { return }
        self.performSegue(withIdentifier: "GotoDetails", sender: loc)
    }
}


//MARK:- Search
extension HomeVC: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    if isSearching {
        viewModel.filterLocations(str: searchController.searchBar.text!)
      }
        self.createSnapshot()
  }
}
