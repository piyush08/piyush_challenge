//
//  SettingsVC.swift
//  PiyushDemo
//
//  Created by Apple on 29/08/21.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var btn_metric : UIButton!
    @IBOutlet weak var btn_imperial : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupData()
    }
    
    func setupData() {
        if let str = UserDefaults.standard.string(forKey: "Units"), str == "imperial" {
            self.buttonPress(btn: btn_imperial)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let unit = btn_imperial.isSelected ? "imperial" : "metric"
        UserDefaults.standard.setValue(unit, forKey: "Units")
    }
    
    @IBAction func buttonPress(btn:UIButton!) {
        btn_metric.isSelected = false
        btn_imperial.isSelected = false
        
        btn.isSelected = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
