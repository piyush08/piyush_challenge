//
//  AddLocationVC.swift
//  PiyushDemo
//
//  Created by Apple on 28/08/21.
//

import UIKit
import MapKit

class AddLocationVC: UIViewController,MKMapViewDelegate {
    
    @IBOutlet weak var mapView : MKMapView!
    
    var viewModel : ViewModel?
    
    let annotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setInitialPin()
    }
    
    func setInitialPin(){
        
        //annotation.coordinate = CLLocationCoordinate2D(latitude: 22.30, longitude: 73.18)
        annotation.coordinate = mapView.centerCoordinate
        annotation.title = "Here"
        
        let coordinateRegion = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 10000000, longitudinalMeters: 1000000)
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.addAnnotation(annotation)
    }
    
    
    /// MapView Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKPointAnnotation {
            
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            //pinAnnotationView.pinTintColor = .purple
            pinAnnotationView.isDraggable = true
            //pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            
            return pinAnnotationView
        }
        
        return nil
    }
    
    /*
     func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
     switch newState {
     case .starting:
     view.dragState = .dragging
     case .ending, .canceling:
     //New cordinates
     //print(view.annotation?.coordinate)
     coOrdinate = view.annotation?.coordinate
     print(coOrdinate)
     view.dragState = .none
     default: break
     }
     }*/
    
    
    @IBAction func saveLocationPress() {
        
        let alert = UIAlertController(title: "Save Location",
                                      message: "Add a new Location",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) {
            [unowned self] action in
            
            guard let textField = alert.textFields?.first,
                  let name = textField.text else {
                return
            }
            self.saveLocation(name: name)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        alert.addTextField()
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
        
    }
    
    func saveLocation(name:String) {
        debugPrint(name)
        debugPrint(annotation.coordinate)
        
        if !name.isEmpty {
            viewModel?.saveData(name: name, coordinate: annotation.coordinate)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
