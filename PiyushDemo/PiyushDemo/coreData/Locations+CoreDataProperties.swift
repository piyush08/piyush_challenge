//
//  Locations+CoreDataProperties.swift
//  PiyushDemo
//
//  Created by Apple on 28/08/21.
//
//

import Foundation
import CoreData


extension Locations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Locations> {
        return NSFetchRequest<Locations>(entityName: "Locations")
    }

    @NSManaged public var name: String
    @NSManaged public var lat: Double
    @NSManaged public var long: Double

}

extension Locations : Identifiable {

}
