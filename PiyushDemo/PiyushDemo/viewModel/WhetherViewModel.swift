//
//  WhetherViewModel.swift
//  PiyushDemo
//
//  Created by Apple on 29/08/21.
//

import Foundation

class WhetherViewModel {
    
    func fetchWhetherData(loc:Locations, completion: @escaping (Result<WhetherModel,Error>)->Void) {
        
        APICall().getWhetherData(lat: loc.lat, long: loc.long) { result in
            completion(result)
        }
        
    }
}
