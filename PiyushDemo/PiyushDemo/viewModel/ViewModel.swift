//
//  ViewModel.swift
//  PiyushDemo
//
//  Created by Apple on 28/08/21.
//

import Foundation
import CoreData

class ViewModel {
    
    var addLocation: ((Locations?) -> Void)?
    
    
    var arrLocations = [Locations]()
    var arrFilterLocations = [Locations]()
    
    
    func saveData(name:String, coordinate: CLLocationCoordinate2D) {
      guard let loc =  CoreDataManager.sharedManager.insertLocation(name: name, coordinate: coordinate)
      else { return }
        
        arrLocations.append(loc)
        if let add = addLocation {
            add(loc)
        }
       
    }
    
    func getAllLocations() -> [Locations] {
        if let arr = CoreDataManager.sharedManager.fetchAllLocations() {
            arrLocations = arr
        }
        return arrLocations
    }
    
    func filterLocations(str:String) {
       arrFilterLocations =  arrLocations.filter({$0.name.lowercased().contains(str.lowercased())})
    }
}
